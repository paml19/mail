<?php
namespace paml\Notification\Mail;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'controllers' => [
        'factories' => [
            Controller\MailController::class => Factory\MailControllerFactory::class,
        ],
    ],
    'controller_plugins' => [
        'factories' => [
            Plugin\MailPlugin::class => Factory\MailPluginFactory::class,
        ],
        'aliases' => [
            'mailer' => Plugin\MailPlugin::class,
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'notification-mail' => [
                    'options' => [
                        'route' => 'notification-mail',
                        'defaults' => [
                            'controller' => Controller\MailController::class,
                            'action' => 'index',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\MailService::class => Factory\MailServiceFactory::class,
            Repository\NotificationRepository::class => Factory\NotificationRepositoryFactory::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'layout/email'            => __DIR__ . '/../view/layout/email.phtml',
            'email/default'           => __DIR__ . '/../view/email/default.phtml'
        ]
    ],
];
