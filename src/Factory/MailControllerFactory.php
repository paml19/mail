<?php

namespace paml\Notification\Mail\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Notification\Mail\Controller\MailController;
use paml\Notification\Mail\Entity\Mail;
use paml\Notification\Mail\Repository\NotificationRepository;
use paml\Notification\Mail\Service\MailService;
use Zend\ServiceManager\Factory\FactoryInterface;

class MailControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new MailController(
            $container->get(EntityManager::class)->getRepository(Mail::class),
            $container->get(MailService::class),
            $container->get('Config')['notification']['items_count'],
            $container->get(NotificationRepository::class)
        );
    }
}
