<?php

namespace paml\Notification\Mail\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Notification\Entity\Notification;
use paml\Notification\Mail\Repository\NotificationRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class NotificationRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): NotificationRepository
    {
        return new NotificationRepository(
            $container->get(EntityManager::class),
            $container->get(EntityManager::class)->getClassMetadata(Notification::class)
        );
    }
}