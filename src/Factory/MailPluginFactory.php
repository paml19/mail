<?php

namespace paml\Notification\Mail\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use paml\Notification\Mail\Entity\Mail;
use paml\Notification\Mail\Plugin\MailPlugin;
use Zend\ServiceManager\Factory\FactoryInterface;

class MailPluginFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new MailPlugin(
            $container->get(EntityManager::class)->getRepository(Mail::class)
        );
    }
}
