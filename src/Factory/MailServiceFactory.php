<?php

namespace paml\Notification\Mail\Factory;

use Interop\Container\ContainerInterface;
use paml\Notification\Mail\Service\MailService;
use Zend\ServiceManager\Factory\FactoryInterface;

class MailServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (! isset($container->get('Config')['notification']['mail'])) {
            throw new \Exception('No notification or mail in config implemented');
        }

        return new MailService(
            $container->get('Config')['view_manager']['template_map'],
            $container->get('Config')['notification']['mail'],
            $container->get('Config')['notification']['app_name']
        );
    }
}
