<?php

namespace paml\Notification\Mail;

class Module
{
    const NOTIFICATION_MAIL_SENT = 'notificationMailSent';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
