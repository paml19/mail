<?php

namespace paml\Notification\Mail\Controller;

use paml\Notification\Mail\Entity\Mail;
use paml\Notification\Mail\Repository\MailRepository;
use paml\Notification\Mail\Service\MailService;
use paml\Notification\Mail\Repository\NotificationRepository;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class MailController extends AbstractConsoleController
{
    private $mailRepository;

    private $mailService;

    private $itemsCount;

    private $notificationRepository;

    public function __construct(
        MailRepository $mailRepository,
        MailService $mailService,
        int $itemsCount,
        NotificationRepository $notificationRepository
    ) {
        $this->mailRepository = $mailRepository;
        $this->mailService = $mailService;
        $this->itemsCount = $itemsCount;
        $this->notificationRepository = $notificationRepository;
    }

    private function prepareMailNotifications()
    {
        $notifications = $this->notificationRepository->findByPlatform(['all', 'mail']);

        foreach ($notifications as $notification) {
            $mail = $this->mailRepository->findOneBy(['notification' => $notification]);

            if (! $mail) {
                $mail = (new Mail)
                    ->setNotification($notification)
                    ->setTitle($notification->getParams()['title'])
                    ->setParams($notification->getParams());

                if (isset($notification->getParams()['mail'])) {
                    $params = $notification->getParams()['mail'];

                    $mail->setRecipients(isset($params['recipients']) ? $params['recipients'] : null)
                        ->setSender(isset($params['sender']) ? $params['sender'] : null)
                        ->setTemplate(isset($params['template']) ? $params['template'] : null)
                        ->setLayout(isset($params['layout']) ? $params['layout'] : null);
                }

                $this->mailRepository->save($mail);
            }
        }
    }

    public function indexAction()
    {
        $this->prepareMailNotifications();

        $mails = $this->mailRepository->findBy(
            [
                'dateSend' => null
            ],
            [
                'dateAdd' => 'ASC'
            ],
            $this->itemsCount
        );

        /** @var Mail $mail */
        foreach ($mails as $mail) {
            if ($mail->getDateAdd() > new \DateTime) {
                continue;
            }

            $error = $this->mailService->send($mail);

            if ($error) {
                $mail->setError($error);
            }

            $mail->setDateSend(new \DateTime);

            $this->mailRepository->save($mail);
        }
    }
}
