<?php

namespace paml\Notification\Mail\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use paml\Notification\Entity\Notification;

/**
 * @ORM\Table(name="notification_mail")
 * @ORM\Entity(repositoryClass="\paml\Notification\Mail\Repository\MailRepository")
 * @GEDMO\Loggable
 * @Gedmo\SoftDeleteable(fieldName="dateDelete", timeAware=false)
 */
class Mail
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="paml\Notification\Entity\Notification")
     * @ORM\JoinColumn(name="notification", referencedColumnName="id")
     */
    private $notification;

    /**
     * @ORM\Column(type="string", name="sender", nullable=true)
     */
    private $sender;

    /**
     * @ORM\Column(type="json_array", name="recipients")
     */
    private $recipients;

    /**
     * @ORM\Column(type="string", name="title", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", name="layout")
     */
    private $layout;

    /**
     * @ORM\Column(type="string", name="template")
     */
    private $template;

    /**
     * @ORM\Column(type="json_array", name="params", nullable=true)
     */
    private $params;

    /**
     * @ORM\Column(type="string", name="error", nullable=true)
     */
    private $error;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_edit", type="datetime", nullable=true)
     */
    private $dateEdit;

    /**
     * @ORM\Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @ORM\Column(type="datetime", name="date_send", nullable=true)
     */
    private $dateSend;

    public function __construct()
    {
        $this->layout = 'layout/email';
        $this->template = 'email/default';
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getNotification(): Notification
    {
        return $this->notification;
    }

    public function setNotification(Notification $notification): self
    {
        $this->notification = $notification;
        return $this;
    }

    public function getSender(): string
    {
        return $this->sender;
    }

    public function setSender(string $sender): self
    {
        $this->sender = $sender;
        return $this;
    }

    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function setRecipients(array $recipients): self
    {
        $this->recipients = $recipients;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getLayout(): string
    {
        return $this->layout;
    }

    public function setLayout(string $layout): self
    {
        $this->layout = $layout;
        return $this;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;
        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): self
    {
        $this->params = $params;
        return $this;
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function setError(string $error): self
    {
        $this->error = $error;
        return $this;
    }

    public function getDateAdd(): \DateTime
    {
        return $this->dateAdd;
    }

    public function getDateEdit(): ?\DateTime
    {
        return $this->dateEdit;
    }

    public function getDateDelete(): ?\DateTime
    {
        return $this->dateDelete;
    }

    public function getDateSend(): ?\DateTime
    {
        return $this->dateSend;
    }

    public function setDateSend(?\DateTime $dateSend): self
    {
        $this->dateSend = $dateSend;
        return $this;
    }
}
