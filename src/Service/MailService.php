<?php

namespace paml\Notification\Mail\Service;

use paml\Notification\Mail\Entity\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Message;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class MailService
{
    private $templateMap;

    private $smtp;

    private $appName;

    public function __construct(array $templateMap, array $smtp, string $appName)
    {
        $this->templateMap = $templateMap;
        $this->smtp = $smtp;
        $this->appName = $appName;
    }

    public function send(Mail $mail)
    {
        if (isset($this->smtp['development_mode'])) {
            return $this->sendWithDevelopmentMode($mail);
        }

        try {
            $body = $this->renderView($mail);

            $message = (new Message)
                ->setEncoding('UTF-8')
                ->addFrom($mail->getSender())
                ->addTo($mail->getRecipients())
                ->setSubject($mail->getTitle())
                ->setBody($body);
            $message->getHeaders()->removeHeader('Content-Type');
            $message->getHeaders()->addHeaderLine('Content-Type', 'text/html; charset=UTF-8');

            $options = new SmtpOptions($this->smtp['smtp']);

            $transport = (new SmtpTransport)
                ->setOptions($options);

            $transport->send($message);
        } catch (\RuntimeException $exception) {
            return $exception->getMessage();
        }
    }

    protected function renderView(Mail $mail): MimeMessage
    {
        $params = (array) $mail->getParams();

        $resolver = (new TemplateMapResolver)
            ->setMap($this->templateMap);

        $renderer = (new PhpRenderer)
            ->setResolver($resolver);

        $viewModel = (new ViewModel)
            ->setTemplate($mail->getTemplate())
            ->setVariables($params);

        $content = $renderer->render($viewModel);

        if ($mail->getLayout()) {
            $variables = array_merge(
                ['content' => $content],
                $params
            );

            $viewLayout = (new ViewModel)
                ->setTemplate($mail->getLayout())
                ->setVariables($variables);

            $content = $renderer->render($viewLayout);
        }

        $html = new MimePart($content);
        $html->type = 'text/html';

        $body = (new MimeMessage)
            ->setParts([$html]);

        return $body;
    }

    protected function sendWithDevelopmentMode(Mail $mail)
    {
        try {
            $body = $this->renderView($mail);
            $options = new SmtpOptions($this->smtp['smtp']);

            $transport = new SmtpTransport();
            $transport->setOptions($options);

            foreach ((array) $mail->getRecipients() as $recipient) {
                $message = (new Message())
                    ->setEncoding('UTF-8')
                    ->addFrom($mail->getSender())
                    ->addTo([$this->smtp['development_recipient']])
                    ->setSubject(vsprintf('[%s] [%s]: %s', [$this->appName, $recipient, $mail->getTitle()]))
                    ->setBody($body);

                $message->getHeaders()->removeHeader('Content-Type');
                $message->getHeaders()->addHeaderLine('Content-Type', 'text/html; charset=UTF-8');

                $transport->send($message);
            }
        } catch (\RuntimeException $exception) {
            return $exception->getMessage();
        }
    }
}
