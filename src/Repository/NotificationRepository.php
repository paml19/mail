<?php

namespace paml\Notification\Mail\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use paml\Notification\Mail\Entity\Mail;
use paml\Notification\Repository\NotificationRepository as MainNotificationRepository;

class NotificationRepository extends MainNotificationRepository
{
    public function findByPlatform(array $platforms): Collection
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $mailsSubQuery = $queryBuilder->select('m')
            ->from(Mail::class, 'm')
            ->where($queryBuilder->expr()->isNull('m.dateDelete'))
            ->getQuery()
            ->getResult();

        $mails = [];

        foreach ($mailsSubQuery as $item) {
            $mails[] = $item->getNotification()->getId();
        }

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select('n')
            ->from($this->getEntityName(), 'n')
            ->where($queryBuilder->expr()->isNull('n.dateDelete'));

        if (! empty($mails)) {
            $queryBuilder->andWhere($queryBuilder->expr()->notIn('n.id', $mails));
        }

        $orX = $queryBuilder->expr()->orX();

        foreach ($platforms as $key => $platform) {
            $orX->add(
                $queryBuilder->expr()->like('n.platforms', '\'%' . $platform . '%\'')
            );
        }

        $queryBuilder->andWhere($orX);

        return new ArrayCollection($queryBuilder->getQuery()->getResult());
    }
}