<?php

namespace paml\Notification\Mail\Repository;

use Doctrine\ORM\EntityRepository;
use paml\Notification\Mail\Entity\Mail;

class MailRepository extends EntityRepository
{
    public function save(Mail $mail): void
    {
        $this->getEntityManager()->persist($mail);
        $this->getEntityManager()->flush();
    }
}
