<?php

namespace paml\Notification\Mail\Plugin;

use paml\Notification\Mail\Entity\Mail;
use paml\Notification\Mail\Repository\MailRepository;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class MailPlugin extends AbstractPlugin
{
    private $mailRepository;

    /** @var Mail */
    private $mail;

    public function __construct(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    public function __invoke(?Mail $mail = null): self
    {
        if ($mail) {
            $this->mailRepository->save($mail);
        } else {
            $this->mail = new Mail;
        }

        return $this;
    }

    public function setSender(string $sender): self
    {
        $this->mail->setSender($sender);
        return $this;
    }

    public function setRecipients(array $recipients): self
    {
        $this->mail->setRecipients($recipients);
        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->mail->setTitle($title);
        return $this;
    }

    public function setLayout(string $layout): self
    {
        $this->mail->setLayout($layout);
        return $this;
    }
    public function setTemplate(string $template): self
    {
        $this->mail->setTemplate($template);
        return $this;
    }

    public function setParams(array $params): self
    {
        $this->mail->setParams($params);
        return $this;
    }

    public function save(): void
    {
        $this->mailRepository->save($this->mail);
    }
}
